const mongoose = require('mongoose')

const bookSchema = new mongoose.Schema({
    bookName:"String",
    image:"string",
    publishedAt: "Date",
    author : "String",
    description : "String",
    price : 'Number'
  });




const Books = mongoose.model('Books', bookSchema);

module.exports = Books